################################################################################
# Package: HltSelChecker
################################################################################
gaudi_subdir(HltSelChecker v12r5)

gaudi_depends_on_subdirs(Event/HltEvent
                         Kernel/HltInterfaces
                         Phys/DaVinciKernel
                         Phys/DaVinciMCKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(HltSelChecker
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES HltEvent HltInterfaces DaVinciKernelLib DaVinciMCKernelLib)

gaudi_install_python_modules()

