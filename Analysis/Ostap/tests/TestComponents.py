#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
# $Id$ 
# =============================================================================
## @file TestComponents.py
#
#  tests for various multicomponents models 
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2014-05-11
# =============================================================================
"""
Tests for various multicomponent models 
"""
# =============================================================================
__version__ = "$Revision:"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2014-05-10"
__all__     = ()  ## nothing to be imported 
# =============================================================================
import ROOT, random
from   Ostap.PyRoUts import *
from   Ostap.Utils   import rooSilent 
# =============================================================================
# logging 
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'Ostap.TesComponents' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
logger.info ( 'Test for multi-component models from Analysis/Ostap')
# =============================================================================
## make simple test mass 
mass    = ROOT.RooRealVar ( 'test_mass' , 'Some test mass' , 0 , 10 )

## book very simple data set
varset  = ROOT.RooArgSet  ( mass )
dataset = ROOT.RooDataSet ( dsID() , 'Test Data set-0' , varset )  

mmin = mass.getMin()
mmax = mass.getMin()

### fill it 
m1 = VE(3,0.500**2)
m2 = VE(5,0.250**2)
m3 = VE(7,0.125**2)

for i in xrange(0,5000) :
    for m in (m1,m2,m3) : 
        mass.setVal ( m.gauss () )
        dataset.add ( varset    )

for i in xrange(0,5000) :
    mass.setVal  ( random.uniform ( mass.getMin() , mass.getMax() ) ) 
    dataset.add ( varset   )

logger.info ('Dataset: %s' % dataset )  
import Ostap.FitModels as Models

signal_1 = Models.Gauss_pdf ( 'G1' , mass = mass  , mean = 3.0 )
signal_2 = Models.Gauss_pdf ( 'G2' , mass = mass  , mean = 5.0 )
signal_3 = Models.Gauss_pdf ( 'G3' , mass = mass  , mean = 7.0 )

wide_1   = Models.Gauss_pdf ( 'GW1', mass = mass  , mean = 4.0  , sigma = 4 )
wide_2   = Models.Gauss_pdf ( 'GW2', mass = mass  , mean = 6.0  , sigma = 4 )

narrow_1 = Models.Gauss_pdf ( 'GN1' , mass = mass , mean = 2.0 , sigma =  1 )
narrow_2 = Models.Gauss_pdf ( 'GN2' , mass = mass , mean = 4.0 , sigma =  1 )
narrow_3 = Models.Gauss_pdf ( 'GN3' , mass = mass , mean = 6.0 , sigma =  1 )
narrow_4 = Models.Gauss_pdf ( 'GN3' , mass = mass , mean = 8.0 , sigma =  1 )

model_ext = Models.Fit1D (
    name = 'Q0' , 
    signal           = signal_1               , 
    othersignals     = [ signal_2 , signal_3 ] ,
    background       = Models.Bkg_pdf ('P' , mass = mass , power = 0 ) ,
    otherbackgrounds = [ wide_1 , wide_2 ] ,
    others           = [ narrow_1 , narrow_2 ,  narrow_3 , narrow_4 ] , 
    )

model_ext.C_1.setVal( 1 )
model_ext.C_2.setVal( 1 )
model_ext.C_3.setVal( 1 )
model_ext.C_4.setVal( 1 )

model_ext.S_1.setVal( 4000 )
model_ext.S_2.setVal( 4000 )
model_ext.S_3.setVal( 4000 )

model_ext.B_1.setVal( 4000 )
model_ext.B_2.setVal(  100 )
model_ext.B_3.setVal(  100 )
model_ext.background.tau.fix(0)

## model_ne = Models.Fit1D (
##     signal              = signal_1                , 
##     othersignals        = [ signal_2 , signal_3 ] ,
##     background          = Models.Bkg_pdf ('P' , mass = mass , power = 0 ) ,
##     otherbackgrounds    = [ wide_1 , wide_2 ] ,
##     others              = [ narrow_1 , narrow_2 ,  narrow_3 , narrow_4 ] , 
##     suffix              = 'NE'   ,
##     extended            = False 
##     )

model_ne1 = Models.Fit1D (
    name = 'Q1' , 
    signal              = signal_1                , 
    othersignals        = [ signal_2 , signal_3 ] ,
    background          = Models.Bkg_pdf ('P' , mass = mass , power = 0 ) ,
    otherbackgrounds    = [ wide_1 , wide_2 ] ,
    others              = [ narrow_1 , narrow_2 ,  narrow_3 , narrow_4 ] , 
    suffix              = 'NE1'  ,
    combine_signals     =  True  ,
    combine_others      =  True  ,
    combine_backgrounds =  True  ,    
    extended = False 
    )

## for m in ( model_ext , model_ne , model_ne1 ) :
for m in ( model_ext , model_ne1 ) :
    ## m.background.tau.fix(0)
    r, f = m.fitTo ( dataset , draw = False , silent = True )
    logger.info ( 'Model %s Fit result \n#%s ' % ( m.name , r ) ) 

# =============================================================================
# The END 
# =============================================================================
