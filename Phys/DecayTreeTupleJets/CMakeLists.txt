################################################################################
# Package: DecayTreeTupleJets
################################################################################
gaudi_subdir(DecayTreeTupleJets v1r4)

gaudi_depends_on_subdirs(Event/HltEvent
                         Phys/DecayTreeTupleBase
                         Phys/LoKiArrayFunctors
                         Phys/JetTagging)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DecayTreeTupleJets
                 src/*.cpp
                 LINK_LIBRARIES HltEvent DecayTreeTupleBaseLib LoKiArrayFunctorsLib)
